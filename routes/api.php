<?php

use App\Http\Controllers\Api\V1\AccountController;
use App\Http\Controllers\Api\V1\AuthController;
use App\Http\Controllers\Api\V1\BlogController;
use App\Http\Controllers\Api\VerifyEmailController;
use Illuminate\Support\Facades\Route;

Route::get('email/verify/{id}/{hash}', [VerifyEmailController::class, '__invoke'])->middleware(['signed', 'throttle:6,1'])->name('verification.verify');
Route::post('email/verify/resend', [VerifyEmailController::class, 'verifyResend'])->middleware(['auth:sanctum', 'throttle:6,1'])->name('verification.send');
Route::get('verified_notice', [VerifyEmailController::class, 'verified_notice'])->name('verification.notice');

Route::prefix('v1')->group(function () {
    Route::get('not_auth', [AuthController::class, 'not_authorized'])->name('login');
    Route::prefix('auth')->group(function () {
        Route::post('register', [AuthController::class, 'register']);
        Route::post('login', [AuthController::class, 'login']);
        Route::get('{provider}', [AuthController::class, 'redirectToProvider']);
        Route::get('{provider}/callback', [AuthController::class, 'handleProviderCallback']);
    });

    Route::prefix('account')->middleware(['auth:sanctum', 'verified'])->group(function () {
        Route::get('profile', [AccountController::class, 'profile']);
    });

    Route::prefix('blogs')->group(function () {
        Route::get('/', [BlogController::class, 'index']);
        Route::post('/', [BlogController::class, 'store']);
        Route::get('/categories', [BlogController::class, 'category']);
        Route::get('/{slug}', [BlogController::class, 'show']);
    });
});
