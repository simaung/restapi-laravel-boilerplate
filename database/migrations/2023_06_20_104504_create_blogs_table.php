<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('blogs', function (Blueprint $table) {
            $table->ulid('id')->primary();
            $table->foreignUlid('category_blog_id')->references('id')->on('category_blogs');
            $table->string('title');
            $table->string('slug')->unique();
            $table->string('image')->default('default.png');;
            $table->string('body');
            $table->enum('is_draft', ['Y', 'N'])->default('Y');
            $table->enum('is_active', ['Y', 'N'])->default('Y');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('blogs');
    }
};
