<?php

namespace App\Services;

use App\Http\Resources\UserResource;
use App\Http\Traits\CommonTrait;
use App\Jobs\SendVerifyEmailJob;
use App\Models\User;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Laravel\Socialite\Facades\Socialite;

class UserService
{
    use CommonTrait;

    public function not_authorized()
    {
        return $this->sendError(401, 'Unauthorised.');
    }

    public function register($request)
    {
        $validator = Validator::make($request->post(), [
            'name' => 'required',
            'phone' => 'required|unique:users',
            'email' => 'required|email|unique:users',
            'password' => 'required',
            'c_password' => 'required|same:password',
            'type' => 'required|in:user,merchant'
        ]);

        if ($validator->fails()) {
            return $this->failsValidate($validator->errors());
        }

        $input = $request->post();
        $input['password'] = bcrypt($input['password']);
        $user = User::create($input);

        if ($user) dispatch(new SendVerifyEmailJob($user));

        return $this->sendResponse([], 'Register successfully');
    }

    public function login($request)
    {
        $validator = Validator::make($request->post(), [
            'email' => 'required',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->failsValidate($validator->errors());
        }

        if (Auth::attempt(
            [
                'email' => $request->email,
                'password' => $request->password
            ]
        )) {
            $user = Auth::user();
            $success['token'] =  $user->createToken(env('APP_NAME'))->plainTextToken;
            $success['user'] =  $user;

            return $this->sendResponse($success, 'Login successfully.');
        }

        return $this->sendError(400, 'Wrong Email/Password.');
    }

    public function profile()
    {
        $user = auth()->user();

        if ($user->is_blocked == 'Y') return $this->sendError(401, 'Account has blocked');

        return $this->sendResponse(new UserResource($user));
    }

    /**
     * Redirect the user to the Provider authentication page.
     *
     * @param $provider
     * @return JsonResponse
     */
    public function redirectToProvider($provider)
    {
        $validated = $this->validateProvider($provider);

        if (!is_null($validated)) {
            return $validated;
        }

        return Socialite::driver($provider)->stateless()->redirect();
    }

    /**
     * Obtain the user information from Provider.
     *
     * @param $provider
     * @return JsonResponse
     */
    public function handleProviderCallback($provider)
    {
        $validated = $this->validateProvider($provider);

        if (!is_null($validated)) {
            return $validated;
        }
        try {
            $user = Socialite::driver($provider)->stateless()->user();
        } catch (ClientException $exception) {
            return $this->sendError(422, 'Invalid credentials provided.');
        }

        $userCreated = User::firstOrCreate(
            [
                'email' => $user->getEmail()
            ],
            [
                'email_verified_at' => now(),
                'name' => $user->getName(),
                'status' => true,
            ]
        );
        $userCreated->socialAccounts()->updateOrCreate(
            [
                'provider_name' => $provider,
                'provider_id' => $user->getId(),
            ],
            [
                'avatar' => $user->getAvatar()
            ]
        );

        $success['token'] =  $userCreated->createToken(env('APP_NAME'))->plainTextToken;
        $success['user'] =  $userCreated;

        return $this->sendResponse($success, 'Login successfully.');
    }

    protected function validateProvider($provider)
    {
        if (!in_array($provider, ['facebook', 'github', 'google'])) return $this->sendError(422, 'Please login using facebook, github or google');
    }
}
