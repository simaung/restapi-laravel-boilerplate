<?php

namespace App\Services;

use App\Http\Resources\BlogCollection;
use App\Http\Resources\BlogResource;
use App\Http\Traits\CommonTrait;
use App\Http\Traits\ImageProcessingTrait;
use App\Models\Blog;
use App\Models\CategoryBlog;
use App\Pipelines\LikeFilter;
use App\Pipelines\LikeRelationFilter;
use Illuminate\Http\Request;
use Illuminate\Pipeline\Pipeline;
use Illuminate\Support\Facades\Validator;

class BlogService
{
    use CommonTrait, ImageProcessingTrait;

    protected $request;
    protected $perPage;

    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->perPage = $request->perPage ?? env('PER_PAGE');
    }

    public function index()
    {
        $blogs = Blog::with(['category'])->active();

        $blogs = app(Pipeline::class)
            ->send($blogs)
            ->through([
                new LikeFilter('title'),
                new LikeFilter('body'),
                new LikeRelationFilter('category', 'category', 'name'),
            ])
            ->thenReturn()
            ->simplePaginate($this->perPage);

        return $this->sendResponse(new BlogCollection($blogs));
    }

    public function show($slug)
    {
        $blogs = Blog::with(['category'])->active()->where('slug', $slug)->first();

        if (!$blogs) return $this->sendError(404, 'Blog Not Found');

        return $this->sendResponse(new BlogResource($blogs));
    }

    public function store()
    {
        $request = $this->request;

        $validator = Validator::make($request->post(), [
            'title' => 'required',
            'body' => 'required',
            'category_blog_id' => 'required',
            'image' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->failsValidate($validator->errors());
        }

        $data = array(
            'title' => $request->title,
            'body' => $request->body,
            'category_blog_id' => $request->category_blog_id,
        );

        // Upload image
        $fileName = $this->uploadImage($request->image, env("STORAGE_PATH") . 'blogs');
        $data['image'] = $fileName;

        Blog::create($data);
        return $this->sendResponse([]);
    }

    public function category()
    {
        $categories = CategoryBlog::active()->get();

        if (!$categories) return $this->sendError(404, 'Category Not Found');

        return $this->sendResponse($categories);
    }
}
