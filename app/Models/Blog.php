<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Concerns\HasUlids;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Blog extends Model
{
    use HasUlids, Sluggable;

    public $incrementing = false;
    protected $keyType = 'string';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $guarded = [];

    public function category()
    {
        return $this->belongsTo(CategoryBlog::class, 'category_blog_id');
    }

    public function scopeActive($query)
    {
        return $query->where(['is_draft' => 'N', 'is_active' => 'Y']);
    }

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    protected function image(): Attribute
    {
        return Attribute::make(
            get: fn (string $value) => config('app.storage_url') . 'blog/' . $value,
        );
    }
}
