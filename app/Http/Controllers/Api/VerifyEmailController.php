<?php

namespace App\Http\Controllers\Api;

use App\Http\Traits\CommonTrait;
use App\Jobs\SendVerifyEmailJob;
use Illuminate\Auth\Events\Verified;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Models\User;

class VerifyEmailController extends Controller
{

    use CommonTrait;

    public function __invoke(Request $request): RedirectResponse
    {
        $user = User::find($request->route('id'));

        if ($user->hasVerifiedEmail()) {
            return redirect(env('FRONT_URL') . '/email/verify/already-success');
        }

        if ($user->markEmailAsVerified()) {
            event(new Verified($user));
        }

        return redirect(env('FRONT_URL') . '/email/verify/success');
    }

    public function verifyResend(Request $request)
    {
        dispatch(new SendVerifyEmailJob($request->user()));

        return $this->sendResponse([], 'Link verification has sent to your email');
    }

    public function verified_notice()
    {
        return $this->sendError(401, 'Please verify your email address');
    }
}
