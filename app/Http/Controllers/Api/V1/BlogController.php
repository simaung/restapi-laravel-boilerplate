<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Services\BlogService;

class BlogController extends Controller
{
    protected $blogService;

    public function __construct(BlogService $blogService)
    {
        $this->blogService = $blogService;
    }

    public function index()
    {
        return $this->blogService->index();
    }

    public function show($slug)
    {
        return $this->blogService->show($slug);
    }

    public function store()
    {
        return $this->blogService->store();
    }

    public function category()
    {
        return $this->blogService->category();
    }
}
