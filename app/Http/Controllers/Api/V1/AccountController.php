<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Services\UserService;

class AccountController extends Controller
{
    public function profile(UserService $userService)
    {
        return $userService->profile();
    }
}
