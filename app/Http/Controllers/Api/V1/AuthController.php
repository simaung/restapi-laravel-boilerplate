<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Services\UserService;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    protected $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function not_authorized()
    {
        return $this->userService->not_authorized();
    }

    public function register(Request $request)
    {
        return $this->userService->register($request);
    }

    public function login(Request $request)
    {
        return $this->userService->login($request);
    }

    public function redirectToProvider($provider)
    {
        return $this->userService->redirectToProvider($provider);
    }

    public function handleProviderCallback($provider)
    {
        return $this->userService->handleProviderCallback($provider);
    }
}
